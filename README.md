# Foundations of Programming in Python

All resources and solutions for the "Foundations of Programming in Python" course at the University of Zurich.

The solutions can be found under "Source" and all resources can be downloaded under "Downloads" on the left.

## Note for Windows users

In order to run Python on Windows you must first install the latest Python interpreter. The Python interpreter can be downloaded here: https://www.python.org/downloads/windows/

## PyCharm

PyCharm is one of the most widespread development environments for Python. The Community Edition is free and can be downloaded here: https://www.jetbrains.com/pycharm/download/

Help for installing and using PyCharm can be found here: https://www.jetbrains.com/pycharm/help/meet-pycharm.html

## Miscellaneous

Interesting article by the Economist about coding that is worth reading: http://www.economist.com/blogs/economist-explains/2015/09/economist-explains-3


# Additional exercises

## Exercise I

Ask for three sentences and print them according to their length (longest first, shortest in the end).

## Exercise II
Given a non-negative number represented as a list of digits,
add 1 to the number ( increment the number represented by the digits ).

Example:

If the input vector is  ```[1, 2, 3]```
the returned vector should be ```[1, 2, 4]```
as 123 + 1 = 124.


```
def plusOne(number_list):
    """
    :param numbers: List[int]
    :return: List[int]
    """
    # Implementation goes here
```

## Exercise III
Write a function that expects a list of integers and checks if the list can be divided into two parts where the sum of the left part equals the sum of the right part. Your function should return True or False.

[5, 6, 2, 3, 4, 6, 6] --> True, [5, 6, 2, 3] and [4, 6, 6]

[4, 5, 6, 7, 8] --> False


## Exercise IV

Create a function that converts a roman number (e.g. "MCMLIV") into an integer (in this case 1954). The function accepts a string as a parameter and returns an integer.

These are the symbols for roman numbers:

- M:    1000
- D: 500
- C: 100
- L: 50
- X: 10
- V: 5
- I: 1

```
def romanToInt(roman_number):
    """
    :param roman_number: str
    :return: int
    """
    # Implementation goes here
```

